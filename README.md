# Postweet
Small web application for tweeting with embedded pictures or videos

## Installation

_NOTE_: This README assumes you already have `npm` and `node` installed in your machine

First of all, pull this repository

    git clone git@bitbucket.org:gcarrasco/postweet.git
    cd postweet

Install the application

    npm install

### Create a twitter app
If you haven't yet, create a [twitter app](https://apps.twitter.com/) and assign it the callback
`http://127.0.0.1:8080/authorized`. You'll need the credentials in the next step

### Create a configuration file
You need to create a configuration file in order to let postweet load your app credentials and allow
users to authenticate with Twitter. Create a file `config/config.json` with the following contents:

```json
{
    "twitter": {
        "consumerKey": "your_app_consumer_key",
    	"consumerSecret": "your_app_consumer_key_secret",
    	"callback": "http://127.0.0.1:8080/authorized"
    }
}
```

## Run the application
Just run `node server.js` and visit `http://localhost:8080` in your browser.
