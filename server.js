// Load necessary modules, configurations and create application
var bodyParser = require('body-parser');
var express = require("express");
var log4js = require("log4js");
var request = require("request");
var twitterAPI = require('node-twitter-api');
var Twit = require('twit');

var config = require('./config/config');

var app = express();

// Configure static files path
app.use(express.static(__dirname + '/public'));

// Increase defaul size for request entities, since we're uploading raw binary data (images/videos)
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.listen(8080);

var logger = log4js.getLogger();
logger.info("App listening on port 8080...");


/////////////////////////////
// Twitter authentication  //
/////////////////////////////

var twitter = new twitterAPI({
	consumerKey: config['twitter']['consumerKey'],
	consumerSecret: config['twitter']['consumerSecret'],
	callback: config['twitter']['callback']
});


// Check weather the user has logged in already or not
app.get('/logged', function(req, res){
  if (app.get('twitterClient'))
    res.status(200).send({'logged': true});
  else
    res.status(200).send({'logged': false});
});

/* Call twitter API for user authentication: https://dev.twitter.com/web/sign-in
*/
app.get('/authorize', function(req, res){
  logger.info("Authorization with Twitter triggered");
  twitter.getRequestToken(function(error, requestToken, requestTokenSecret, results){
  	if (error) {
  		logger.error("Error getting requestToken and requestTokenSecret : " + error);
      res.status(503).send(error);
  	} else {
      // Save credentials at application level for further use
      app.set('requestToken', requestToken);
      app.set('requestTokenSecret', requestTokenSecret);
      logger.info("requestToken and requestTokenSecret obtained successfully");

      // Send response to the client
      var authUrl = twitter.getAuthUrl(requestToken);
  		var result = {'requestToken': requestToken, 'requestTokenSecret': requestTokenSecret, 'authUrl': authUrl};
      res.json(result);
  	}
  });

});

/* Callback that Twitter will call when user gives authorization
*/
app.get('/authorized', function(req, res) {
  var requestToken = app.get('requestToken');
  var requestTokenSecret = app.get('requestTokenSecret');
  var oauth_verifier = req.query.oauth_verifier;

  twitter.getAccessToken(requestToken, requestTokenSecret, oauth_verifier, function(error, accessToken, accessTokenSecret, results) {
  	if (error) {
      logger.error("Error getting access token : " + error);
      res.status(503).send(error);
  	} else {
  		//store accessToken and accessTokenSecret for firther use in requests
      logger.info("User authentication successful");
      app.set('accessToken', accessToken);
      app.set('accessTokenSecret', accessTokenSecret);

      // Create a Twitter client object
      var client = new Twit({
        consumer_key: config['twitter']['consumerKey'],
        consumer_secret: config['twitter']['consumerSecret'],
        access_token: accessToken,
        access_token_secret: accessTokenSecret
      });
      app.set('twitterClient', client);

      // Set cookie so that the frontend knows the user is already authenticated
      res.cookie('authenticated', true);

      // Redirect to the index page
      res.sendFile(__dirname + '/index.html');
  	}
  });
});


/////////////////////////////////
// API and application routes  //
/////////////////////////////////
app.get('/', function(req, res){
  res.sendFile(__dirname + "/index.html");
});


/* POST a tweet through Twitter API. If the request contains the key mediaIds, then the media will
   be attached to the Tweet. The media has been previously uploaded.
*/
app.post('/api/v1/tweet', function(req, res) {
  var client = app.get('twitterClient');

  var tweetContent = {'status': req.body.statusText};
  if (req.body.mediaIds) {
    tweetContent['media_ids'] = req.body.mediaIds;
  }

  client.post('statuses/update', tweetContent, function(error, tweet, response){
    if (error) {
      logger.error(error);
      res.status(503).send(error);
    }
    else {
      logger.info("Tweet posted correctly");
      res.status(200).send(tweet);
    }
  });

});


// Upload media using twitter API, reference https://dev.twitter.com/rest/reference/post/media/upload
app.post('/api/v1/media/:step', function(req, res){

  var client = app.get('twitterClient');

  // Step 1: INIT media upload
  if(req.params.step == 'INIT') {
    var params = {
      'command': 'INIT',
      'total_bytes': req.body.mediaSize,
      'media_type': req.body.mediaType
    };
    uploadMediaCall("Upload initialization correct", params, res);
  }

  // Step 2: APPEND media upload
  else if(req.params.step == 'APPEND') {
    var params = {
      'command': 'APPEND',
      'media_id': req.body.mediaId,
      'media_data': req.body.mediaData,
      'segment_index': 0
    };
    uploadMediaCall("Upload appending correct", params, res);
  }

  // Step 3: FINALIZE media upload
  else if(req.params.step == 'FINALIZE') {
    var params = {
      'command': 'FINALIZE',
      'media_id': req.body.mediaId
    };
    uploadMediaCall("Upload finalizing correct", params, res);
  }

  else {
    res.status(404).send("Bad request");
  }

});


////////////////////
// Helper methods //
////////////////////

// Call media/upload and hanlde error and log message. Solves lots of code repetition
function uploadMediaCall(logMessage, params, response) {
  var client = app.get('twitterClient');
  client.post('media/upload', params, function(error, media, res){
    if(!error) {
      logger.info(logMessage);
      response.status(200).send(media);
    }
    else {
      logger.error(error);
      response.status(503).send(error.Error);
    }
  });
}
