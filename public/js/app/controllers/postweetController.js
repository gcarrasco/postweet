// Helper method to log into the UI panel and the console
function panelLogger(message) {
  var date = new Date();
  message = '[' + date.toISOString() + ']: ' + message + '\n';
  console.log(message);
  $("#logPanel").append(message);
}

angular.module('postweet')
.controller('postweetController', ['$http', '$window', '$cookies', function($http, $window, $cookies) {

  panelLogger("App ready");

  // Variable used to keep track of the currently uploaded file
  this.fileData = {};

  // Save controller instance in a variable to be able to use it within $http module
  var ctrl = this;

  // Angular UI Double-binging objects
  this.statusText = "";

  // Reset local variables and UI components
  function resetData() {
    ctrl.statusText = "";
    $("#fileInput")[0].value = '';
    ctrl.fileData = {};
  };


  // Check if user is already authenticated with twitter in the app
  $http.get('/logged').success(function(response){
    ctrl.authenticated = response.logged;
  }).error(function(response) {
    panelLogger("There was a problem checking if the user is authenticated with the app: " + response.message);
  });

  // Twitter authentication flow
  this.beginTwitterAuth = function() {
    panelLogger("Starting Twitter authorization");
    $http.get('/authorize')
      .success(function(response){
        panelLogger("Getting access token: successful");
        ctrl.requestToken = response.requestToken;
        ctrl.requestTokenSecret = response.requestTokenSecret;
        ctrl.authUrl = response.authUrl;

        panelLogger("Asking user for permission to authorize Postweet");
        $window.location.href = ctrl.authUrl;
      })
      .error(function(response){
        panelLogger("Something went wrong with twitter authentication: " + response.message);
    });
  };


  this.postTweet = function() {
    // Prepare tweet JSON object
    var tweet_data = {'statusText': ctrl.statusText};

    // If the user loaded a media file, it will be set in the controller
    if (ctrl.fileData['media_id']) {
      tweet_data['mediaIds'] = ctrl.fileData['media_id'];
    }

    panelLogger("POSTing tweet with content: " + ctrl.statusText);
    $http.post('/api/v1/tweet', tweet_data).success(function(data){
      panelLogger("Tweet posted correctly!");
      alert("Tweet posted correctly! :-) ");
      // Reset forms to be able to post another tweet
      resetData();
    }).error(function(response){
      panelLogger("Something went wrong when posting the tweet: " + response.message);
      resetData();
    });

  };

  var fileInput = document.getElementById('fileInput');

  /* Listener for the File input, will upload the media to Twitter when selected, so that its available
   when the user selects "Post tweet!" button.

   Uploading media to twitter is done in three steps as descrived in the API documentation:
   https://dev.twitter.com/rest/media/uploading-media
  */
  fileInput.addEventListener('change', function(e) {

    // Local method to avoid code repetition when submitting error
    function uploadError(response) {
      panelLogger("Something went wrong when uploading media to twitter: " + response.message);
      // Re-enable the buttos if media upload failed so that the user can still post a tweet
      $("#postButton").attr('disabled', false);
      resetData();
    };

    // Disable post tweet button while loading the media
    $("#postButton").attr('disabled', true);

    panelLogger("Uploading selected file");
		var file = fileInput.files[0];
		var reader = new FileReader();

    reader.onload = function(e) {
      var mediaData = reader.result.split(',')[1];
      var params = {
        'mediaType': file.type,
        'mediaSize': file.size
      };

      // Step 1: Send INIT request with basic file information
      panelLogger('Initialize media upload to twitter');
      $http.post('/api/v1/media/INIT', params).success(function(response){
        panelLogger('Initialize media upload to twitter: success');
        ctrl.fileData['media_id'] = response.media_id_string;
        params = {
          'mediaId': ctrl.fileData['media_id'],
          'mediaData': mediaData
        };

        // Step 2: Upload (APPEND) file data
        panelLogger('Start upload media to twitter');
        $http.post('/api/v1/media/APPEND', params).success(function(response){
          panelLogger('Start upload media to twitter: success');

          // Step 3: Finalize media upload
          panelLogger("Finalizing request upload");
          $http.post('/api/v1/media/FINALIZE', params).success(function(response){
            panelLogger("Media uploaded correctly!");
            shortenTextArea();
            $("#postButton").attr('disabled', false);
          }).error(uploadError);
        }).error(uploadError);
      }).error(uploadError);
    };
    reader.readAsDataURL(file);
	});

  // If user uploads media, the amount of characters that can be posted in a tweet is reduced by 20 - 25
  function shortenTextArea() {
    var maxLength = 140 - 25;
    ctrl.statusText = ctrl.statusText.substr(0, maxLength);
    $("#tweetText").attr('maxlength', maxLength);
  };

}]);
